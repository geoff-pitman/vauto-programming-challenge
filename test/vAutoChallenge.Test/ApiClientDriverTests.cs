﻿/*
Author: Geoffrey Pitman
Date: 12/24/2017
Purpose: vAuto Programming Challenege
Challenge Description: 
    Using the provided API, create a program that retrieves a datasetID,
    retrieves all vehicles and dealers for that dataset, and successfully 
    posts to the answer endpoint. Each vehicle and dealer should be
    requested only once. You will receive a response structure when you 
    post to the answer endpoint that describes status and total ellapsed time; 
    your program should output this response.
*/
using NUnit.Framework;

namespace vAutoChallenge.Test
{
    /// <summary>
    /// This is a text fixture for the ApiClientDriver class.
    /// </summary>
    [TestFixture]
    public class ApiClientDriverTests
    {
        ApiClientDriver apiClientDriver;

        /// <summary>
        /// Setup before each unit test.
        /// </summary>
        [SetUp]
        public void Init()
        {
            // Assemble
            apiClientDriver = new ApiClientDriver();
        }

        /// <summary>
        /// Clean up after each unit test.
        /// </summary>
        [TearDown]
        public void Cleanup()
        {
            apiClientDriver.Dispose();
        }

        /// <summary>
        /// Tests the CompleteChallenege() method.
        /// </summary>
        [Test]
        public void CompleteChallengeTest()
        {
            // Act
            apiClientDriver.CompleteChallenge();

            // Assert
            Assert.IsTrue(apiClientDriver.AnswerResponse.Success.Value, 
                "FAILURE:\n" + apiClientDriver.AnswerResponse.ToJson());

            // Assert
            Assert.Less(apiClientDriver.AnswerResponse.TotalMilliseconds,
                24000, "FAILURE: Took too long!\n" + apiClientDriver.AnswerResponse.ToJson());
        }
    }
}
