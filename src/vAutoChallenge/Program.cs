﻿/*
Author: Geoffrey Pitman
Date: 12/24/2017
Purpose: vAuto Programming Challenege
Challenge Description: 
    Using the provided API, create a program that retrieves a datasetID,
    retrieves all vehicles and dealers for that dataset, and successfully 
    posts to the answer endpoint. Each vehicle and dealer should be
    requested only once. You will receive a response structure when you 
    post to the answer endpoint that describes status and total ellapsed time; 
    your program should output this response.
*/
using System;

namespace vAutoChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiClientDriver = new ApiClientDriver();
            string result = apiClientDriver.CompleteChallenge();
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
