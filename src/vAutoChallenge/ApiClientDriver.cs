﻿/*
Author: Geoffrey Pitman
Date: 12/24/2017
Purpose: vAuto Programming Challenege
Challenge Description: 
    Using the provided API, create a program that retrieves a datasetID,
    retrieves all vehicles and dealers for that dataset, and successfully 
    posts to the answer endpoint. Each vehicle and dealer should be
    requested only once. You will receive a response structure when you 
    post to the answer endpoint that describes status and total ellapsed time; 
    your program should output this response.
*/
using IO.Swagger.Api;
using IO.Swagger.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace vAutoChallenge
{
    public class ApiClientDriver : IDisposable
    {
        /// <summary>
        /// Private reference to the answer response.
        /// </summary>
        AnswerResponse _answerResponse = new AnswerResponse();

        /// <summary>
        /// The answer response.  Used in unit tests.
        /// </summary>
        public AnswerResponse AnswerResponse
        {
            get
            {
                return _answerResponse;
            }
        }

        /// <summary>
        /// The api reference for retrieveing the dataset id and for posting the answer.
        /// </summary>
        readonly DataSetApi datasetApi = new DataSetApi();

        /// <summary>
        /// The dataset id.
        /// </summary>
        string datasetId;

        /// <summary>
        /// The list of dealer answers.
        /// </summary>
        List<DealerAnswer> dealerAnswerList = new List<DealerAnswer>();

        /// <summary>
        /// The api reference for retrieving dealer information.
        /// </summary>
        readonly DealersApi dealersApi = new DealersApi();

        /// <summary>
        /// Holds references to dealer request tasks.
        /// </summary>
        List<Task> dealerTaskList = new List<Task>();

        /// <summary>
        /// Dictionary used to associate vehicles with their respective dealer.
        /// </summary>
        Dictionary<int?, List<VehicleAnswer>> dealerVehicleDict = new Dictionary<int?, List<VehicleAnswer>>();

        /// <summary>
        /// The api reference for retrieving vehicle information.
        /// </summary>
        readonly VehiclesApi vehiclesApi = new VehiclesApi();

        /// <summary>
        /// The GetVehicleIds api response.
        /// </summary>
        VehicleIdsResponse vehicleIds;

        /// <summary>
        /// Holds references to vehicle request tasks.
        /// </summary>
        List<Task> vehicleTaskList = new List<Task>();

        /// <summary>
        /// Completes the vAuto programming challenge.
        /// </summary>
        /// <returns>The programming challenege result.</returns>
        public string CompleteChallenge()
        {
            try
            {
                // Make api request calls.
                InitiateApiCalls();

                // Post answer.
                _answerResponse = PostAnswer();

                // Return response result.
                return _answerResponse.ToJson();
            }
            catch (Exception ex)
            {
                return "FAILURE:\n" + ex.Message;
            }
        }

        /// <summary>
        /// Thanks the code reviewer.
        /// </summary>
        public void Dispose()
        {
            var sb = new StringBuilder();
            sb.Append("==========================================================");
            sb.Append("\n   Thank you for taking the time to look at my code!\n");
            sb.Append("==========================================================");
            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Gets information for a dealer.
        /// </summary>
        /// <param name="dealerId">The dealer id.</param>
        /// <returns>The task.</returns>
        Task GetDealerInfo(int? dealerId)
        {
            return Task.Run(() =>
            {
                // Api call GetDealer request.
                var dealResponse = dealersApi.DealersGetDealer(datasetId, dealerId);
                var dealerAnswer = new DealerAnswer(dealResponse.DealerId, dealResponse.Name,
                    new List<VehicleAnswer>());
                dealerAnswerList.Add(dealerAnswer);
            });
        }

        /// <summary>
        /// Gets information for a vehicle.
        /// </summary>
        /// <param name="vehicleId">The vehicle id.</param>
        /// <returns>The task.</returns>
        Task GetVehicleInfo(int? vehicleId)
        {
            return Task.Run(() =>
            {
                // Api call GetVehicle request.
                var vehicleResponse = vehiclesApi.VehiclesGetVehicle(datasetId, vehicleId);
                var vehicleAnswer = new VehicleAnswer(vehicleResponse.VehicleId, vehicleResponse.Year,
                    vehicleResponse.Make, vehicleResponse.Model);

                // If the dealer Id key exists, add the vehicle answer.
                if (dealerVehicleDict.ContainsKey(vehicleResponse.DealerId))
                {
                    dealerVehicleDict[vehicleResponse.DealerId].Add(vehicleAnswer);
                }
                // Otherwise, add dealer key first, then add the answer.
                else
                {
                    dealerVehicleDict.Add(vehicleResponse.DealerId, new List<VehicleAnswer>());
                    dealerVehicleDict[vehicleResponse.DealerId].Add(vehicleAnswer);
                    
                    // Get dealer info and add to task list.
                    dealerTaskList.Add(GetDealerInfo(vehicleResponse.DealerId));
                }
            });
        }

        /// <summary>
        /// Initiates the api calls.
        /// </summary>
        void InitiateApiCalls()
        {
            // Make api call to get the data set id.
            datasetId = datasetApi.DataSetGetDataSetId().DatasetId;

            // Make api call to get the vehicle ids.
            vehicleIds = vehiclesApi.VehiclesGetIds(datasetId);

            // Request vehicle info for all vehicles.
            foreach (var vehicleId in vehicleIds.VehicleIds)
            {
                // Spin up a new thread for each GetVehicle request and add it to the
                // vehicles thread list.
                var getVehicleInfoTask = GetVehicleInfo(vehicleId);
                vehicleTaskList.Add(getVehicleInfoTask);
            }

            // Wait for all the threads to finish up.
            Task.WaitAll(vehicleTaskList.ToArray());
            Task.WaitAll(dealerTaskList.ToArray());
        }

        /// <summary>
        /// Posts the challenge answer.
        /// </summary>
        /// <returns>The answer response.</returns>
        AnswerResponse PostAnswer()
        {
            // Add vehicle answer lists to dealer answers.
            foreach (var dealer in dealerAnswerList)
            {
                dealer.Vehicles = dealerVehicleDict[dealer.DealerId];
            }
            var answer = new Answer(dealerAnswerList);
            return datasetApi.DataSetPostAnswer(datasetId, answer);
        }
    }
}

